#ifndef _CLASSES_H
#define _CLASSES_H

#include <Adafruit_Arcada.h>

#define RADIUS_DEFAULT 15
#define RADIUS_MAX 40
#define RADIUS_MIN 5
#define STICK_SENSITIVITY 100
#define STICK_DEADZONE 50
#define DIALOG_PAUSE "- Paused -"
#define BACKGROUND ARCADA_BLACK


struct previous_t
{
    int x;
    int y;
    int r;
};

struct display_geo_t
{
    int min_x;
    int min_y;
    int max_x;
    int max_y;
};


class Ball {
    public:
        int x, y, r;
        uint16_t color;

        Ball(Adafruit_SPITFT*);
        void init(int, int, int, uint16_t);
        void init(uint16_t);
        void init(void);

        void draw_update(void);
        void draw_previous(void);
        void draw(void);

        void move_to(int, int);
        void reset(void);
        void inc_radius(void);
        void dec_radius(void);

    private:
        display_geo_t disp_geo;
        previous_t prev;
        Adafruit_SPITFT* display;
};


class Game {
    public:
        Game(Adafruit_Arcada);
        void init(void);
        void step(void);
        
    private:
        Adafruit_Arcada arcada;
        uint32_t buttons_pressed, buttons_released;
        int16_t stick_x, stick_y;
        Ball *ball_p;

        void handle_buttons(void);
        void handle_stick(void);
};


#endif
