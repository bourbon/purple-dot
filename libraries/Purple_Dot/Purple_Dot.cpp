#include <Adafruit_Arcada.h>
#include "Purple_Dot.h"


Game::Game(Adafruit_Arcada arcada) {
    this->arcada = arcada;
}

void Game::init(void) {
    arcada.arcadaBegin();
    arcada.displayBegin();
    arcada.display->fillScreen(BACKGROUND);
    arcada.setBacklight(200);

    ball_p = new Ball(arcada.display);
    ball_p->init();
}

void Game::step(void) {
    handle_buttons();
    handle_stick();
}

void Game::handle_buttons(void) {
    arcada.readButtons();
    buttons_pressed = arcada.justPressedButtons();
    buttons_released = arcada.justReleasedButtons();

    if (buttons_released & ARCADA_BUTTONMASK_START) {
        arcada.infoBox(DIALOG_PAUSE, ARCADA_BUTTONMASK_START);
        arcada.display->fillScreen(ARCADA_BLACK);
        ball_p->draw();
    }
    if (buttons_released & ARCADA_BUTTONMASK_SELECT) {
        ball_p->reset();
    }
}

void Game::handle_stick(void) {
    stick_x = arcada.readJoystickX();
    stick_y = arcada.readJoystickY();

    if ((abs(stick_x) > STICK_DEADZONE) || (abs(stick_y) > STICK_DEADZONE)) {
        ball_p->move_to(stick_x / STICK_SENSITIVITY, 
                        stick_y / STICK_SENSITIVITY);
    }
}

Ball::Ball(Adafruit_SPITFT *display) {
    this->display = display;
    disp_geo.min_x = 0;
    disp_geo.min_y = 0;
    disp_geo.max_x = display->width() - 1;
    disp_geo.max_y = display->height() - 1;
}

void Ball::init(int x, int y, int r, uint16_t color) {
    this->x = prev.x = x;
    this->y = prev.y = y;
    this->r = prev.r = r;
    this->color = color;
    draw();
}
void Ball::init(uint16_t color) {
    init(disp_geo.max_x / 2, disp_geo.max_y / 2, RADIUS_DEFAULT, color);
}
void Ball::init(void) {
    init(ARCADA_PURPLE);
}

void Ball::draw_update(void) {
    // do nothing if no change
    if (x == prev.x && y == prev.y && r == prev.r) return;

    // fill old circle and draw new
    draw_previous();
    draw();

    // update previous values
    prev.x = x;
    prev.y = y;
    prev.r = r;
}
void Ball::draw(void) {
    display->fillCircle(x, y, r, color);
}
void Ball::draw_previous(void) {
    display->fillCircle(prev.x, prev.y, prev.r, BACKGROUND);
}

void Ball::move_to(int x, int y) {
    int new_x = this->x + x;
    int new_y = this->y + y;

    // prevent exiting the screen
    if (new_x + r > disp_geo.max_x) new_x = disp_geo.max_x - r;
    if (new_x - r < disp_geo.min_x) new_x = disp_geo.min_x + r;
    if (new_y + r > disp_geo.max_y) new_y = disp_geo.max_y - r;
    if (new_y - r < disp_geo.min_y) new_y = disp_geo.min_y + r;

    // set new position and display
    this->x = new_x;
    this->y = new_y;
    draw_update();
}

void Ball::reset(void) {
    x = disp_geo.max_x / 2;
    y = disp_geo.max_y / 2;
    r = RADIUS_DEFAULT;
    color = ARCADA_PURPLE;
    draw_update();
}

void Ball::inc_radius(void) {
    if (++r > RADIUS_MAX) r = RADIUS_MAX;
    draw_update();
}
void Ball::dec_radius(void) {
    if (--r < RADIUS_MIN) r = RADIUS_MIN;
    draw_update();
}
