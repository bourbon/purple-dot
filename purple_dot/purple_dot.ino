#include "Purple_Dot.h"


Adafruit_Arcada arcada;
Game game = Game(arcada);


void setup(void) {
    game.init();
}

void loop() {
    game.step();
    delay(25);
}
